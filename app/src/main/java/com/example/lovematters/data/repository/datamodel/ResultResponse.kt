package com.example.lovematters.data.repository.datamodel

import com.google.gson.annotations.SerializedName

data class ResultResponse(
    @SerializedName("first name: ")
    val firstName :String,

    @SerializedName("second name: ")
    val secondName :String,

    @SerializedName("percentage match: ")
    val percentageMatch :Double,

    @SerializedName("result: ")
    val result :String
)
