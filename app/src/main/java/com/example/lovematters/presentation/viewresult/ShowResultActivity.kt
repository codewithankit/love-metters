package com.example.lovematters.presentation.viewresult

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.lovematters.R
import com.example.lovematters.data.repository.datamodel.ResultResponse
import com.example.lovematters.databinding.ActivityShowResultBinding
import com.example.lovematters.util.Keys
import com.google.gson.Gson
import java.io.File
import java.io.OutputStream
import kotlin.math.roundToInt


@Suppress("DEPRECATION")
class ShowResultActivity : AppCompatActivity() {
    private lateinit var binding : ActivityShowResultBinding
    private lateinit var loverResult : ResultResponse
    private lateinit var imagePath : File


    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_show_result)
        binding.lifecycleOwner = this

        val tempIntent = intent
        loverResult =
            Gson().fromJson(tempIntent.getStringExtra(Keys.LOVERS), ResultResponse::class.java)
        setData(loverResult)
    }

    @SuppressLint("NewApi")
    private fun setData(loverResult : ResultResponse) {
        val percentage = loverResult.percentageMatch.roundToInt()
        val withSign = "$percentage%"
        binding.tvFirstName.text = loverResult.firstName
        binding.tvSecondName.text = loverResult.secondName
        binding.tvParcentageResult.text = withSign
        binding.tvMassege.text = loverResult.result
        binding.btnShareResult.setOnClickListener {
            val bitmap : Bitmap? = takeScreenshot()
            storeImageAsJPEGandShare(bitmap!!)
         //   saveBitmap(bitmap!!)
            //      shareIt()

        }
    }

    private fun takeScreenshot() : Bitmap? {
        val rootView = findViewById<View>(android.R.id.content).rootView
        rootView.isDrawingCacheEnabled = true
        return rootView.drawingCache
    }


    //----------------------------------------------------------------------------------------------
    @SuppressLint("Recycle")
    @RequiresApi(Build.VERSION_CODES.R)
    private fun storeImageAsJPEGandShare(bitmap:Bitmap){
       val outPutStream:OutputStream
//       try {
//           //-------------------scoped storage is support after Q
//           if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.Q)
//           {
//              val contentResolver : ContentResolver = contentResolver
//              val contentValues:ContentValues = ContentValues()
//               contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME,"Image_"+".jpg")
//               contentValues.put(MediaStore.MediaColumns.MIME_TYPE,"image/jpeg")
//               contentValues.put(MediaStore.QUERY_ARG_RELATED_URI,Environment.DIRECTORY_PICTURES + File.separator + "TestFolder")
//               val imageUri: Uri? =contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,contentValues)
//               outPutStream= contentResolver.openOutputStream(Objects.requireNonNull(imageUri!!))!!
//               bitmap.compress(Bitmap.CompressFormat.JPEG,100 ,outPutStream)
//               Objects.requireNonNull(outPutStream)
//               Toast.makeText(this, " Image Save ", Toast.LENGTH_SHORT).show()
//
//               val share = Intent(Intent.ACTION_SEND)
//               share.type = "Image/jpeg"
//               share.putExtra(Intent.EXTRA_STREAM,imageUri)
//               startActivity(Intent.createChooser(share," Share Via "))
//           }
//
//       } catch (){
//
//       }

    }














//    private fun saveBitmap(bitmap : Bitmap) {
//        imagePath = File(
//            Environment.getExternalStorageDirectory().toString() + "/scrnshot.png"
//        ) ////File imagePath
//        val fos : FileOutputStream
//        try {
//            fos = FileOutputStream(imagePath)
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
//            fos.flush()
//            fos.close()
//        } catch (e : FileNotFoundException) {
//            Toast.makeText(this, "${e.message}", Toast.LENGTH_SHORT).show()
//        } catch (e : IOException) {
//            Toast.makeText(this, "${e.message}", Toast.LENGTH_SHORT).show()
//        }
//    }


//    private fun shareIt() {
//        val uri = Uri.fromFile(imagePath)
//        val sharingIntent = Intent(Intent.ACTION_SEND)
//        sharingIntent.type = "image/*"
//        val shareBody = " Lovers Love Percentage with screen shot"
//        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Lover's Love ")
//        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
//        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri)
//        startActivity(Intent.createChooser(sharingIntent, "Share via"))
//    }


}