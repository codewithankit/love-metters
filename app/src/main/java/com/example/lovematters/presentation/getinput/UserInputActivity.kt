package com.example.lovematters.presentation.getinput

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.lovematters.R
import com.example.lovematters.databinding.ActivityUserInputBinding
import com.example.lovematters.presentation.genderselection.ChooseGenderActivity
import com.example.lovematters.presentation.viewresult.ShowResultActivity
import com.example.lovematters.util.Keys
import com.google.gson.Gson

class UserInputActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding : ActivityUserInputBinding
    private lateinit var viewModel : UserInputActivityViewModel

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_input)
        viewModel = ViewModelProvider(this)[UserInputActivityViewModel::class.java]
        val tempIntent = intent
        val getResult = tempIntent.getStringExtra(Keys.GENDER)
        if (getResult == "female") {
            binding.imagePerson.setImageResource(R.drawable.femalepng)
        } else {
            binding.imagePerson.setImageResource(R.drawable.maleimage)
        }
        binding.imageBackBottonInput.setOnClickListener(this)
        binding.btnCalculateNow.setOnClickListener(this)
    }

    override fun onClick(v : View?) {
        when (v?.id) {
            R.id.btn_calculate_now -> {
                val yourName = binding.yourNameInput.text.toString()
                val partenerName = binding.partnerNameInput.text.toString()
                viewModel.getResult(yourName, partenerName).observe(this){
                    val intent=Intent(this,ShowResultActivity::class.java)
                    intent.putExtra(Keys.LOVERS, Gson().toJson(it))
                    startActivity(intent)
                }
            }

            R.id.image_back_botton_input -> {
                startActivity(
                    Intent(
                        this@UserInputActivity,
                        ChooseGenderActivity::class.java
                    )
                )
            }

        }
    }
}