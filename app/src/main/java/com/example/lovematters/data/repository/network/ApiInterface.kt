package com.example.lovematters.data.repository.network

import com.example.lovematters.data.repository.datamodel.ResultResponse
import com.example.lovematters.util.Constants.Companion.API_KEY
import com.example.lovematters.util.Constants.Companion.HOST
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiInterface {
    @Headers("X-RapidAPI-Key:$API_KEY","X-RapidAPI-Host:$HOST")
    @GET("love-calculator")
    fun getResult(
    @Query("fname") yourName:String,
    @Query("sname") partenerName:String
    ):Call<ResultResponse>
}