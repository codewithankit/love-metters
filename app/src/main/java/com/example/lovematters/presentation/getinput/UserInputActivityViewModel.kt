package com.example.lovematters.presentation.getinput

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.lovematters.data.repository.datamodel.ResultResponse
import com.example.lovematters.data.repository.datarepository.LoveCalculatorDataRepository

class UserInputActivityViewModel : ViewModel(){
    private val loveCalculatorDataRepository=LoveCalculatorDataRepository()

    fun getResult(yourName:String, partenerName:String):LiveData<ResultResponse>{
        return loveCalculatorDataRepository.getResult(yourName, partenerName)
    }
}