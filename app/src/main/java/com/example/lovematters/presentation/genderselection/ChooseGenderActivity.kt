package com.example.lovematters.presentation.genderselection

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.example.lovematters.R
import com.example.lovematters.databinding.ActivityChooseGenderBinding
import com.example.lovematters.presentation.getinput.UserInputActivity
import com.example.lovematters.presentation.propose.ProposeActivity
import com.example.lovematters.util.Keys

class ChooseGenderActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding:ActivityChooseGenderBinding
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this,R.layout.activity_choose_gender)
        binding.imageFemale.setOnClickListener(this)
        binding.imageMale.setOnClickListener(this)
        binding.imageBackBotton.setOnClickListener(this)
    }

    override fun onClick(v : View?) {
        when(v?.id){
            R.id.image_female->{
                val intent= Intent(this@ChooseGenderActivity, UserInputActivity::class.java)
                intent.putExtra(Keys.GENDER,"female")
                startActivity(intent)

            }
            R.id.image_male->{
                val intent=Intent(this@ChooseGenderActivity, UserInputActivity::class.java)
                intent.putExtra(Keys.GENDER,"male")
                startActivity(intent)
            }
            R.id.image_back_botton->{
                startActivity(Intent(this@ChooseGenderActivity, ProposeActivity::class.java))
            }
        }
    }
}