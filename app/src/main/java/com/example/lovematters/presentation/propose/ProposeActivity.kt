package com.example.lovematters.presentation.propose

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.lovematters.R
import com.example.lovematters.databinding.ActivityProposeBinding
import com.example.lovematters.presentation.genderselection.ChooseGenderActivity
import com.example.lovematters.presentation.welcome.WelComeActivity

class ProposeActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding:ActivityProposeBinding
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this,R.layout.activity_propose)
        binding.imageHeartStart.setOnClickListener(this)
        binding.btnShare.setOnClickListener(this)
        binding.btnRateUs.setOnClickListener(this)
        binding.imgBackArrow.setOnClickListener(this)
    }

    override fun onClick(v : View?) {
        when(v?.id){
            R.id.img_back_arrow-> {
                startActivity(Intent(this@ProposeActivity, WelComeActivity::class.java))
            }
            R.id.image_heart_start->{
                startActivity(Intent(this@ProposeActivity, ChooseGenderActivity::class.java))
            }
            R.id.btn_rate_us->{
                Toast.makeText(this@ProposeActivity, " Click Rate Us ", Toast.LENGTH_SHORT).show()
            }
            R.id.btn_share->{
                Toast.makeText(this@ProposeActivity, " Click Share App", Toast.LENGTH_SHORT).show()
            }
        }
    }
}