package com.example.lovematters.presentation.welcome

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.lovematters.R
import com.example.lovematters.databinding.ActivityWelComeBinding
import com.example.lovematters.presentation.propose.ProposeActivity

class WelComeActivity : AppCompatActivity() , View.OnClickListener {

    private lateinit var binding:ActivityWelComeBinding
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this,R.layout.activity_wel_come)
        binding.btnLoveQuetue.setOnClickListener(this)
        binding.btnTestLove.setOnClickListener(this)
        binding.imageShare.setOnClickListener(this)
        binding.imageStar.setOnClickListener(this)
    }

    override fun onClick(v : View?) {
        when(v?.id){
            R.id.btn_love_quetue->{
                Toast.makeText(this@WelComeActivity, " Love Qoutes Click ", Toast.LENGTH_SHORT).show()
            }
            R.id.btn_test_love->{
                startActivity(Intent(this@WelComeActivity, ProposeActivity::class.java))
            }
            R.id.image_share->{
                Toast.makeText(this@WelComeActivity, " Click on Share ", Toast.LENGTH_SHORT).show()
            }
            R.id.image_star->{
                Toast.makeText(this@WelComeActivity, " Click on Rate Us  ", Toast.LENGTH_SHORT).show()
            }
        }
    }
}