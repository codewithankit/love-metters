package com.example.lovematters.data.repository.datarepository

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.lovematters.data.repository.datamodel.ResultResponse
import com.example.lovematters.data.repository.network.RetrofitInstance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoveCalculatorDataRepository  {
    private var loversResponseData: MutableLiveData<ResultResponse> = MutableLiveData()
    fun getResult(yourName:String,partenerName:String):LiveData<ResultResponse>{
        val call=RetrofitInstance.apiInterface.getResult(yourName,partenerName)
        call.enqueue(object :Callback<ResultResponse>{

            override fun onResponse(
                call : Call<ResultResponse>,
                response : Response<ResultResponse>,
            ) {
                if (response.isSuccessful) {
                    val data=response.body()
                    loversResponseData.postValue(data!!)
                }
            }


            override fun onFailure(call : Call<ResultResponse>, t : Throwable) {

            }

        })
        return loversResponseData
    }

}
